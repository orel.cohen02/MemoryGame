"use strict";

window.onload =
    function () {

        /************** Board Data **************/

        // Data
        var boardTiles;
        var boardSize;
        var tilesFlipped;
        var lastFlipped;
        var pairsFound;

        // Consts
        var images = ["android.png", "angular.png", "css.png", "git.png", "html.png", "js.png", "node.png", "react.png"];
        var firstChoiceSize = 4;
        var secondChoiceSize = 6;
        var thirdChoiceSize = 8;
        var playTimeInMillis = 120000;
        var cardImagesClass = "cardImage";
        var animationDurationMillis = 4000;


        /****************** Modal *************************/

        (function handleModal() {

            drawBoard(document.getElementById('firstChoiceTb'), firstChoiceSize);
            drawBoard(document.getElementById('secondChoiceTb'), secondChoiceSize);
            drawBoard(document.getElementById('thirdChoiceTb'), thirdChoiceSize);

            // Get the modal
            var modal = document.getElementById('chooseBoardModal');

            // Get the <span> element that closes the modal
            var closeBtn = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            closeBtn.onclick = function () {
                hideModal(modal);
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    hideModal(modal);
                }
            }

            document.getElementById("openModal").addEventListener("click", function () {
                (document.getElementById('chooseBoardModal')).style.visibility = "visible";
            });
        })();

        function hideModal(modal) {
            modal.style.visibility = "hidden";
            var tablesList = document.getElementsByClassName("choice-table");
        }


        /******************* General **********************/

        // General - invokes a function on each cell of the tiles matrix
        function invokeFunctionOnBoard(func) {
            boardTiles.forEach(function (tileRow) {
                tileRow.forEach(func);
            });
        }

        // draws a general table with the size given in the element given
        function drawBoard(htmlTable, size) {
            var htmlRow;
            var htmlCell;
            for (var row = 0; row < size; row++) {

                htmlRow = htmlTable.insertRow();
                for (var col = 0; col < size; col++) {
                    htmlCell = htmlRow.insertCell();
                }
            }
        }


        /********************** Starting a new game *********************/

        (function setEventListeners() {

            // start game button is disbled in the beginning
            // enabled after choosing a board size
            // disabled again after clicking the button
            document.getElementById("startGame").disabled = true;

            document.getElementById("startGame")
                .addEventListener("click", function () {
                    document.getElementById("startGame").disabled = true;
                });

            (Array.from(document.getElementsByClassName("choice-table"))).map(function (htmlTable) {
                htmlTable.addEventListener("click", function () {
                    document.getElementById("startGame").disabled = false;
                });
            })

            document.getElementById("startGame")
                .addEventListener("click", startGame); /* Start game btn */

            function handleTableChosen(size) {
                hideModal(document.getElementById('chooseBoardModal'));
                setBoard(size);
            }

            // table sizes choices
            document.getElementById("firstChoiceTb")
                .addEventListener("click", function () { handleTableChosen(firstChoiceSize) });
            document.getElementById("secondChoiceTb")
                .addEventListener("click", function () { handleTableChosen(secondChoiceSize) });
            document.getElementById("thirdChoiceTb")
                .addEventListener("click", function () { handleTableChosen(thirdChoiceSize) });
        })();

        function initializeData() {
            boardTiles = [];
            tilesFlipped = 0;
            lastFlipped = null;
            pairsFound = 0;

            // clear the html table
            document.getElementById("memoryBoard").innerHTML = '';

            if (refreshTimer != undefined) {
                endTimer();
            }
        }

        // function initializes the board data and draws a new one
        function setBoard(size) {
            initializeData();
            boardSize = size;
            generateValues();
            shuffleTiles();
            newBoard();
        }

        // filling the values array
        function generateValues() {
            for (var i = 0; i < boardSize; i++) {

                boardTiles.push([]);

                for (var j = 0; j < boardSize; j++) {

                    boardTiles[i].push({
                        urlIndex: (Math.floor(((i * boardSize + j) / 2)) % boardSize),
                        val: i * boardSize + j + 1, /* This acts as the id of the td's in the html document*/
                        clicksAmounts: 0,
                        flipped: false
                    })
                }

            }
        };

        // shuffling the values array
        function shuffleTiles() {
            boardTiles.forEach(function (tileRow) {
                tileRow.forEach(function (tile) {
                    var currRow = boardTiles.indexOf(tileRow);
                    var currColumn = tileRow.indexOf(tile);

                    var randomRow = Math.floor((Math.random() * boardSize));
                    var randomColumn = Math.floor((Math.random() * boardSize));;

                    var temp = boardTiles[randomRow][randomColumn];
                    boardTiles[randomRow][randomColumn] = tile;
                    boardTiles[currRow][currColumn] = temp;
                });
            });
        };

        // injecting the tiles matrix to table in the html
        function newBoard() {

            var htmlTable = document.getElementById("memoryBoard");

            boardTiles.forEach(function (tilesrow) {

                var htmlRow = htmlTable.insertRow();
                htmlRow.id = "row_" + boardTiles.indexOf(tilesrow);

                tilesrow.forEach(function (tile) {

                    var htmlCell = htmlRow.insertCell();
                    htmlCell.id = tile.val;
                    htmlCell.disabled = true;
                    htmlCell.addEventListener("click", function () { checkAndFlip(tile) });


                    var tileImg = document.createElement("IMG");
                    tileImg.setAttribute("src", images[tile.urlIndex]);
                    tileImg.className = cardImagesClass;

                    tileImg.style.visibility = "hidden";
                    htmlCell.appendChild(tileImg);

                    htmlCell.addEventListener("mouseover", function () {
                        if (!htmlCell.disabled) {
                            htmlCell.style.backgroundColor = "#99ccff";
                        }
                    });
                    htmlCell.addEventListener("mouseout", function () {
                        htmlCell.style.backgroundColor = "#3399ff";
                    });
                });

            });
        };

        // flips all cards for 2 seconds and starts timer
        function startGame() {

            invokeFunctionOnBoard(flipTile);
            setTimeout(function () {
                invokeFunctionOnBoard(flipTile);
                invokeFunctionOnBoard(function (tile) {
                    document.getElementById(tile.val).disabled = false;
                });
                addAnimationClasses();
                
                setTimeout(startTimer, animationDurationMillis);
            }, 2000);
        }

        function addAnimationClasses() {

            boardTiles.forEach(function (tilesRow) {
                let htmlRow = document.getElementById("row_" + boardTiles.indexOf(tilesRow));

                if (boardTiles.indexOf(tilesRow) % 2 === 0) {
                    htmlRow.className = "even-row";
                }
                else {
                    htmlRow.className = "odd-row";
                }

                let rowCells = htmlRow.childNodes;

                tilesRow.forEach(function (tile) {

                    let htmlCell = rowCells[tilesRow.indexOf(tile)];

                    if (tilesRow.indexOf(tile) % 2 === 0) {
                        htmlCell.className = "even-cell";
                    }
                    else {
                        htmlCell.className = "odd-cell";
                    }
                })
            });
        }

        /*******************Playing a game *********************************/

        function flipTile(tile) {

            let tileImageElement = document.getElementById(tile.val).getElementsByClassName(cardImagesClass)[0];

            if (!tile.flipped) {
                tileImageElement.style.visibility = "visible";
            } else {
                tileImageElement.style.visibility = "hidden";
            }

            tile.flipped = !tile.flipped;
        }

        // function invoked when the user clicks a cell
        function checkAndFlip(tile) {

            var htmlCell = document.getElementById(tile.val);

            if (!htmlCell.disabled) {

                tile.clicksAmounts++;

                // no tiles have been flipped by now
                if (tilesFlipped === 0) {
                    flipTile(tile);
                    lastFlipped = tile;
                    tilesFlipped++;
                }

                // another tile that isn't this one was already flipped
                else if (tilesFlipped === 1 && lastFlipped.val !== tile.val) {

                    flipTile(tile);
                    document.getElementById('memoryBoard').style.pointerEvents = 'none';

                    // tiles do not match
                    if (lastFlipped.urlIndex !== tile.urlIndex) {
                        setTimeout(function () {
                            flipTile(tile);
                            flipTile(lastFlipped);
                            lastFlipped = null;
                            tilesFlipped = 0;
                            document.getElementById('memoryBoard').style.pointerEvents = 'auto';
                        }, 500);
                        // tiles do not match
                    } else {
                        htmlCell.disabled = true;
                        pairsFound++;
                        document.getElementById(lastFlipped.val).disabled = true;
                        lastFlipped = null;
                        tilesFlipped = 0;
                        document.getElementById('memoryBoard').style.pointerEvents = 'auto';

                        if (pairsFound == ((boardSize * boardSize) / 2)) {
                            setTimeout(function () { endGame(victory); }, 500);
                        }
                    }
                }
            }
        }

        /*********************** Ending a game ****************************/

        function endGame(resultFunc) {
            initializeData();
            endTimer();
            resultFunc();
        }

        function victory() {
            window.alert("winner");
        }

        function loss() {
            window.alert("loser");
        }

        /************** Timer **************/

        var startTime = new Date(playTimeInMillis);
        document.getElementById("timer").innerHTML = dateToString(startTime);
        var refreshTimer;

        // handler function
        function startTimer(e) {
            startTime = new Date(playTimeInMillis);
            refreshTimer = setInterval(function () { MoveTime() }, 1000);
        }

        function MoveTime() {
            startTime = (new Date(startTime - (new Date(1000))));
            var timeString = dateToString(startTime);
            if (timeString === "00:00") {
                endTimer();
                endGame(loss);
                return;
            }
            document.getElementById("timer").innerHTML = timeString;
        };

        function endTimer() {
            clearInterval(refreshTimer);
            startTime = new Date(playTimeInMillis);
            document.getElementById("timer").innerHTML = dateToString(startTime);
        }

        function dateToString(date) {
            var timeString = date.toUTCString().split(' ')[4];
            timeString = timeString.split(":")[1] + ":" + timeString.split(":")[2];
            return timeString;
        }
    };